import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel3/config/asset.dart';
import 'package:project_kel3/event/event_db.dart';
import 'package:project_kel3/model/pengajuan.dart';
import 'package:project_kel3/screen/admin/list_pengajuan.dart';
import 'package:project_kel3/widget/info.dart';

class AddUpdatePengajuan extends StatefulWidget {
  final Pengajuan? pengajuan;
  AddUpdatePengajuan({this.pengajuan});

  @override
  State<AddUpdatePengajuan> createState() => _AddUpdatePengajuanState();
}

class _AddUpdatePengajuanState extends State<AddUpdatePengajuan> {
  var _formKey = GlobalKey<FormState>();
  var _controllerKodePengajuan = TextEditingController();
  var _controllerTanggal = TextEditingController();
  var _controllerNpmPeminjam = TextEditingController();
  var _controllerNamaPeminjam = TextEditingController();
  var _controllerProdi = TextEditingController();
  var _controllerNoHandphone = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengajuan != null) {
      _controllerKodePengajuan.text = widget.pengajuan!.kodePengajuan!;
      _controllerTanggal.text = widget.pengajuan!.tanggal!;
      _controllerNpmPeminjam.text = widget.pengajuan!.npmPeminjam!;
      _controllerNamaPeminjam.text = widget.pengajuan!.namaPeminjam!;
      _controllerProdi.text = widget.pengajuan!.prodi!;
      _controllerNoHandphone.text = widget.pengajuan!.noHandphone!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengajuan != null
            ? Text('Update pengajuan')
            : Text('Tambah pengajuan'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengajuan == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerKodePengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerTanggal,
                  decoration: InputDecoration(
                      labelText: "Tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNpmPeminjam,
                  decoration: InputDecoration(
                      labelText: "NPM Peminjam",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNamaPeminjam,
                  decoration: InputDecoration(
                      labelText: "Nama Peminjam",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerProdi,
                  decoration: InputDecoration(
                      labelText: "Prodi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerNoHandphone,
                  decoration: InputDecoration(
                      labelText: "No Handphone",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengajuan == null) {
                        String message = await EventDb.AddPengajuan(
                            _controllerKodePengajuan.text,
                            _controllerTanggal.text,
                            _controllerNpmPeminjam.text,
                            _controllerNamaPeminjam.text,
                            _controllerProdi.text,
                            _controllerNoHandphone.text);
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerKodePengajuan.clear();
                          _controllerTanggal.clear();
                          _controllerNpmPeminjam.clear();
                          _controllerNamaPeminjam.clear();
                          _controllerProdi.clear();
                          _controllerNoHandphone.clear();
                          Get.off(
                            ListPengajuan(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengajuan(
                          _controllerKodePengajuan.text,
                          _controllerTanggal.text,
                          _controllerNpmPeminjam.text,
                          _controllerNamaPeminjam.text,
                          _controllerProdi.text,
                          _controllerNoHandphone.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengajuan == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
