import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel3/config/asset.dart';
import 'package:project_kel3/event/event_db.dart';
import 'package:project_kel3/model/pengembalian.dart';
import 'package:project_kel3/screen/admin/list_pengembalian.dart';
import 'package:project_kel3/widget/info.dart';

class AddUpdatePengembalian extends StatefulWidget {
  final Pengembalian? pengembalian;
  AddUpdatePengembalian({this.pengembalian});

  @override
  State<AddUpdatePengembalian> createState() => _AddUpdatePengembalianState();
}

class _AddUpdatePengembalianState extends State<AddUpdatePengembalian> {
  var _formKey = GlobalKey<FormState>();
  var _controllerKodePengembalian = TextEditingController();
  var _controllerKodePengajuan = TextEditingController();
  var _controllerTanggalKembali = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengembalian != null) {
      _controllerKodePengembalian.text = widget.pengembalian!.kodePengembalian!;
      _controllerKodePengajuan.text = widget.pengembalian!.kodePengajuan!;
      _controllerTanggalKembali.text = widget.pengembalian!.tanggalKembali!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengembalian != null
            ? Text('Update pengembalian')
            : Text('Tambah pengembalian'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengembalian == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerKodePengembalian,
                  decoration: InputDecoration(
                      labelText: "Kode Pengembalian",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerKodePengajuan,
                  decoration: InputDecoration(
                      labelText: "Kode Pengajuan",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerTanggalKembali,
                  decoration: InputDecoration(
                      labelText: "Tanggal Kembali",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengembalian == null) {
                        String message = await EventDb.AddPengembalian(
                            _controllerKodePengembalian.text,
                            _controllerKodePengajuan.text,
                            _controllerTanggalKembali.text);
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerKodePengembalian.clear();
                          _controllerKodePengajuan.clear();
                          _controllerTanggalKembali.clear();
                          Get.off(
                            ListPengembalian(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengembalian(
                          _controllerKodePengembalian.text,
                          _controllerKodePengajuan.text,
                          _controllerTanggalKembali.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengembalian == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
