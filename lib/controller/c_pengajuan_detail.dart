import 'package:get/get.dart';
import 'package:project_kel3/model/pengajuan_detail.dart';

class CPengajuan_detail extends GetxController {
  Rx<Pengajuan_detail> _pengajuan_detail = Pengajuan_detail().obs;

  Pengajuan_detail get pengajuan_detail => _pengajuan_detail.value;

  void setpengajuan_detail(Pengajuan_detail dataPengajuan_detail) =>
      _pengajuan_detail.value = dataPengajuan_detail;
}
