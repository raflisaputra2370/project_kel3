import 'package:get/get.dart';
import 'package:project_kel3/model/pengembalian_detail.dart';

class CPengembalian_detail extends GetxController {
  Rx<Pengembalian_detail> _pengembalian_detail = Pengembalian_detail().obs;

  Pengembalian_detail get pengembalian_detail => _pengembalian_detail.value;

  void setpengembalian_detail(Pengembalian_detail dataPengembalian_detail) =>
      _pengembalian_detail.value = dataPengembalian_detail;
}
