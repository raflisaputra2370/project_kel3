class Api {
  static const _host = "http://192.168.100.203/api_kel_3";

  static String _user = "$_host/user";
  static String _barang = "$_host/barang";
  static String _pengajuan = "$_host/pengajuan";
  static String _pengembalian = "$_host/pengembalian";
  static String _pengajuan_detail = "$_host/pengajuan_detail";
  static String _pengembalian_detail = "$_host/pengembalian_detail";

  static String login = "$_host/login.php";

  // user
  static String addUser = "$_user/add_user.php";
  static String deleteUser = "$_user/delete_user.php";
  static String getUsers = "$_user/get_users.php";
  static String updateUser = "$_user/update_user.php";

  // barang
  static String addBarang = "$_barang/add_barang.php";
  static String deleteBarang = "$_barang/delete_barang.php";
  static String getBarang = "$_barang/get_barang.php";
  static String updateBarang = "$_barang/update_barang.php";

  // pengajuan
  static String addPengajuan = "$_pengajuan/add_pengajuan.php";
  static String deletePengajuan = "$_pengajuan/delete_pengajuan.php";
  static String getPengajuan = "$_pengajuan/get_pengajuan.php";
  static String updatePengajuan = "$_pengajuan/update_pengajuan.php";

  // pengembalian
  static String addPengembalian = "$_pengembalian/add_pengembalian.php";
  static String deletePengembalian = "$_pengembalian/delete_pengembalian.php";
  static String getPengembalian = "$_pengembalian/get_pengembalian.php";
  static String updatePengembalian = "$_pengembalian/update_pengembalian.php";

  // pengajuan_detail
  static String addPengajuanDetail =
      "$_pengajuan_detail/add_pengajuan_detail.php";
  static String deletePengajuanDetail =
      "$_pengajuan_detail/delete_pengajuan_detail.php";
  static String getPengajuanDetail =
      "$_pengajuan_detail/get_pengajuan_detail.php";
  static String updatePengajuanDetail =
      "$_pengajuan_detail/update_pengajuan_detail.php";

  // pengembalian_detail
  static String addPengembalianDetail =
      "$_pengembalian_detail/add_pengembalian_detail.php";
  static String deletePengembalianDetail =
      "$_pengembalian_detail/delete_pengembalian_detail.php";
  static String getPengembalianDetail =
      "$_pengembalian_detail/get_pengembalian_detail.php";
  static String updatePengembalianDetail =
      "$_pengembalian_detail/update_pengembalian_detail.php";
}
