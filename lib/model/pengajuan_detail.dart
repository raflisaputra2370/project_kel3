class Pengajuan_detail {
  String? kodepengajuan;
  String? kodebarang;
  String? jumlah;

  Pengajuan_detail({
    this.kodepengajuan,
    this.kodebarang,
    this.jumlah,
  });

  factory Pengajuan_detail.fromJson(Map<String, dynamic> json) =>
      Pengajuan_detail(
        kodepengajuan: json['kode_pengajuan'],
        kodebarang: json['kode_barang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kode_pengajuan': this.kodepengajuan,
        'kode_barang': this.kodebarang,
        'jumlah': this.jumlah,
      };
}
