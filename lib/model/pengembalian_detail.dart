class Pengembalian_detail{
  String? kodepengembalian;
  String? kodebarang;
  String? jumlah;


  Pengembalian_detail({
    this.kodepengembalian,
    this.kodebarang,
    this.jumlah,
  });

  factory Pengembalian_detail.fromJson(Map<String, dynamic> json) => Pengembalian_detail(
        kodepengembalian: json['kodepengembalian'],
        kodebarang: json['kodebarang'],
        jumlah: json['jumlah'],
      );

  Map<String, dynamic> toJson() => {
        'kodepengembalian_detail': this.kodepengembalian,
        'kodebarang': this.kodebarang,
        'jumlah': this.jumlah,
      };
}
