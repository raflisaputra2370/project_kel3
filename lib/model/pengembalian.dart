class Pengembalian {
  String? kodePengembalian;
  String? kodePengajuan;
  String? tanggalKembali;

  Pengembalian({
    this.kodePengembalian,
    this.kodePengajuan,
    this.tanggalKembali,
  });

  factory Pengembalian.fromJson(Map<String, dynamic> json) => Pengembalian(
        kodePengembalian: json['kode_pengembalian'],
        kodePengajuan: json['kode_pengajuan'],
        tanggalKembali: json['tanggal_kembali'],
      );

  Map<String, dynamic> toJson() => {
        'kode_pengembalian': this.kodePengembalian,
        'kode_pengajuan': this.kodePengajuan,
        'tanggal_kembali': this.tanggalKembali,
      };
}
