import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:project_kel3/config/asset.dart';
import 'package:project_kel3/model/user.dart';
import 'package:project_kel3/screen/admin/dashboard_admin.dart';
import 'package:project_kel3/screen/login.dart';
import 'package:get/get_navigation/get_navigation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      theme: ThemeData(
        primaryColor: Asset.colorPrimaryDark,
        scaffoldBackgroundColor: Colors.white,
      ),
      debugShowCheckedModeBanner: false,
      home: FutureBuilder(builder: (context, AsyncSnapshot<User?> snapshoot) {
        return DashboardAdmin();
        // return Login();
      }),
    );
  }
}
